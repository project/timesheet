<?php


$path_to_pdf_class = drupal_get_path('module','timesheet') . '/print/fpdf/';
$path_to_pdf_fonts = $path_to_pdf_class . 'font/'; 

define('FPDF_FONTPATH', $path_to_pdf_fonts);
define('COPYRIGHT_CHAR', chr(169));
define('EM_DASH', chr(151));

require($path_to_pdf_class . 'fpdf.php');