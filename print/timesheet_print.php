<?php

  
function _timesheet_print_pdf ($timesheet, $tid, $end_date, $end_date_formatted, $user_name, $uid, $title, $sub_title, $footer, $show_zeros) {
  define('FOOTER', $footer);
  define('SHOW_HEADER', TRUE);
  define('SHOW_FOOTER', TRUE);
  
  require('timesheet_print.class.php');
  
  $pdf= new timesheet_pdf(TRUE, TRUE);
  $pdf->AddPage();
  $_LARGE_FONT_SIZE = '16';
  $_NORMAL_FONT_SIZE = '10';
  $_SMALL_FONT_SIZE = '8';
  $_TINY_FONT_SIZE = '6';
  $_SPACING = '5';

  // Print the Header
  $pdf->set_header(TRUE);
  
  // Print the Users Informatoin
  $pdf->SetFont('Arial', 'B', $_LARGE_FONT_SIZE);
  $pdf->Cell(185, $_SPACING, $title, 0, 0);
  $pdf->SetFont('Arial', 'B', $_NORMAL_FONT_SIZE);
  $pdf->Cell(0, $_SPACING, $user_name, 0, 1); 
  $pdf->SetFont('Arial', '', $_NORMAL_FONT_SIZE);
  $pdf->Cell(0, $_SPACING, $sub_title, 0, 1);
  $pdf->Ln();
  $pdf->Cell(185, $_SPACING, 'Timesheet ID: ' . $tid . ' ' . EM_DASH . ' ' . $end_date_formatted, 0, 1);
  $pdf->Ln();
  $pdf->SetFont('Arial', '', $_NORMAL_FONT_SIZE);
  
  $headers = array('Project', 'Labor Category', 'On Site');
  $current_date = $end_date - (60 * 60 * 24 * 13);
  for ($i = 0; $i < 14; $i++) { 
    $headers[] = date('jS', $current_date);
    $current_date += 86400;
  }
  $headers[] = 'Total';
  $rows = array();
  $col_total = array();
  $off_set = '3';
  foreach($timesheet as $project) {
    $row_total = 0;
    
    foreach($project as $k => $v) {
      
      switch($k){
        case 'pid':
          $row = array();
          $proj_name = timesheet_get_projects($project['pid']);
          $proj_name = $proj_name['title'];
          $row['pid'] = $proj_name;
          break;
        case 'lid':
          $labor = timesheet_get_labor_categories($project['lid']);
          $labor = $labor['name'];
          $row['lid'] = $labor;
          break;
        case 'on_site':
          if($row['on_site']) {
            $row['on_site'] = "Yes";
          }
          else {
            $row['on_site'] = "No";
          }
          break;
        default:
          if (!$show_zeros) {
            $v = ($v == 0) ? '' : ((intval($v) == $v) ? intval($v) : $v);
          }
          $row[$k] = $v;
          $row_total += $v;
          $col_total[$k] += $v;
          break;
      }
      
    }
    // Totals
    $row['total'] = $row_total; 
    $rows[] = $row;
    
  }
  
  $col_total_row = array('Total');
  for ($i = 1; $i < $off_set; $i++) {
    $col_total_row[] = '';
  }
  foreach ($col_total as $key => $v) {
    $col_total_row[] = $v;
  }
  $col_total_row[] = array_sum($col_total_row);
  $rows[] = $col_total_row;
  
  $pdf->SetFont('Arial','B','8');
  
  $pdf->BasicTable($headers, $rows);
  
  $pdf->Output();
}

