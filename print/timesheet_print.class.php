<?php


require_once('pdf_setup.inc');

$_header = FALSE;
$_footer = FALSE;

class timesheet_pdf extends FPDF {

  function timesheet_pdf($header = false, $footer = false, $orientation = 'L', $unit = 'mm', $format = 'letter') {
    $this->_header = $header;
    $this->_footer = $footer;
    $this->AliasNbPages();
    $this->FPDF($orientation, $unit, $format);
  }

  // Page Header
  function Header() {
    return;
  }
  
  //Page footer
  function Footer() {
    if(!$this->_footer) {
      return;
    }
    //Position at 1.5 cm from bottom
    $this->SetY(-20);
    $this->SetFont('Arial', 'B', '8');
    $this->set_color('normal');
    $this->Cell(0, 1, FOOTER, 0, 1);
  }

  function set_header($set) {
    $this->_header = $set;
  }
  
  function set_footer($set) {
    $this->_footer = $set;
  }
    
  //Simple table
  function BasicTable($header, $data) {
    $default_w = '10';
    $padding = 2;
    
    $new_header = array();
    foreach($header as $k => $info) {
      if (is_array($info)) {
        $new_header[] = $info['title'];
      }
      else {
        $new_header[] = $info;
      }
      $w[] = $this->getStringWidth($info) + $padding;
    }
    $header = $new_header;
  
    
    // Check the Data for large rows
    foreach($data as $row) {
      $c = 0;
      foreach($row as $cell) {
        $w[$c] = $this->getStringWidth($cell) > $w[$c] ? $this->getStringWidth($cell) + $padding  : $w[$c];
        $c++;
      }
    }
    
    //Header
    for($i = 0; $i < count($header); $i++) {
      $this->set_color('total');
      $this->Cell($w[$i], 7, $header[$i], 'LRBT', 0, 'R', 1);
    }
  
    $this->Ln();
    
    //Data
    $r = 0;
    foreach ($data as $row) {
      $j = 0;
      
      foreach ($row as $cell) {
        //if last row
        if ($r == count($data) - 1) {
          $this->set_color('total');
        }
        else if ($r % 2 == 0 && $j % 2 == 0) {
          $this->set_color('on');
        }
        else if ($r % 2 != 0) {
          $this->set_color('off');
        }
        else {
          $this->set_color('off2');
        }
      
        $this->Cell($w[$j], 8, $cell, 'LRBT', 0, 'R', 1);
        $j++;  
      }
      $this->Ln();
      $r++;
    }
    //Closure line
    $this->Cell(array_sum($w), 0, '', 'T');
  }

  function set_color($type = 'normal') {
    switch($type) {
      case 'on':
        $this->SetFillColor(255, 255, 255);
        $this->SetTextColor(0);
        $this->SetDrawColor(120, 120, 120);
        $this->SetLineWidth(.3);
        $this->SetFont('');
        break;
      case 'off':
        $this->SetFillColor(190, 190, 190);
        $this->SetTextColor(0);
        $this->SetDrawColor(120, 120, 120);
        $this->SetLineWidth(.3);
        $this->SetFont('');
        break;
      case 'off2':
        $this->SetFillColor(230, 230, 230);
        $this->SetTextColor(0);
        $this->SetDrawColor(120, 120, 120);
        $this->SetLineWidth(.3);
        $this->SetFont('');
        break;
      case 'total':
        $this->SetFillColor(120, 120, 120);
        $this->SetTextColor(255);
        $this->SetDrawColor(120, 120, 120);
        $this->SetLineWidth(.3);
        $this->SetFont('', 'B');
        break;
     case 'normal':
     default:
      $this->SetFillColor(255, 255, 255);
      $this->SetTextColor(0);
      $this->SetFont('');
      break;
    }
    return;
  } 
}

