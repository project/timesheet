
var total = 0;
var focus = false;
                   
// Global killswitch
if (isJsEnabled()) {
  addLoadEvent(timesheetAutoAttach);
}


/**
 * Attaches an onkeyup event behaviour to all required forms
 */
function timesheetAutoAttach() {
  var forms = document.forms;
  var f, form, element, td;
  for (var i = 0; form = forms[i]; i++) {
    if (form && hasClass(form, 'timesheetCheck')) {
      var tds = form.getElementsByTagName('td');
      for (var k = 0; td = tds[k]; k++) {
        if (hasClass(td, 'cell-highlight')) {
          //need to redefine to maks JS happy
          f = form;
          td.onmouseover = function() { if (!focus) { highlight(f, this.id, 'add'); } }
          td.onmouseout = function() { if (!focus) { highlight(f, this.id, 'remove'); } }
        }
      }
      for (var j = 0; element = form[j]; j++) {
        if (element.type && element.type.toLowerCase() == 'text') {
          //need to redefine to maks JS happy
          f = form;
          element.onkeyup = function() { validate(this, this.form); }
          element.onfocus = function() { highlight(f, this.id, 'add'); focus = true;}
          element.onclick = function() { this.select(); }
          element.onblur = function() { highlight(f, this.id, 'remove'); focus = false; if (!timesheetShowZeros) { c = this.value; this.value = (c == 0) ? '' : ((parseInt(c) == c) ? parseInt(c) : c); validate(this, this.form); } }
        }
		    //add javascript checks if the submit or save button was clicked
        else if (element.type && element.type.toLowerCase() == 'submit') {
          element.onclick = function() {return timesheetSubmit(this.form);}
		    }
		  }
      //fix an extraneous input data that hasn't been saved but form has been reloaded
      computeTotals(form);
    }
  }
}


/**
 * Validates an input timesheet value
 */
function validate(e, f) {
  //valid numbers: 0-24 with increments of .5 allowed
  var numberCheck= /^(([01]?[0-9]|2[0-3])?(\.[0-9])?|24\.0|24)?$/;

  //if input is good or it's empty (equivalent to 0)
  if (numberCheck.test(e.value) || e.value == '') {
    removeClass(e, 'error');
	  computeTotals(f);
  }
  else {
    addClass(e, 'error');
  }
}


/**
 * Compute all of the total fields
 */
function computeTotals(f) {
  var totalDays = [['w1_sun', 0], ['w1_mon', 0], ['w1_tue', 0], ['w1_wed', 0], ['w1_thu', 0], ['w1_fri', 0], ['w1_sat', 0], 
                   ['w2_sun', 0], ['w2_mon', 0], ['w2_tue', 0], ['w2_wed', 0], ['w2_thu', 0], ['w2_fri', 0], ['w2_sat', 0]];                 
  var total = 0;
  var cell, row, e;
  
  //don't count the timesheet fieldset
  var n = f.getElementsByTagName('fieldset').length - 1;

  //if there is the add project fieldset make sure we don't count that
  if ($('timesheet-add-project') != null) n--;

  for (var i = 0; i < n; i++) {
    for (var j = 0, row = 0; j < totalDays.length; j++) {
      e = 'edit-timesheet-project-' + i + '-days-' + totalDays[j][0];
      
      //if the element doesn't exist return, this would be the case if the form elements are disabled when a timesheet has been unsubmitted
      if (f[e]) {
        cell = parseFloat(f[e].value);
      }
      else {
        return;
      }

      if (!isNaN(cell)) {     
        row += cell;
        totalDays[j][1] += cell;
 
        //round to 1 decimal place
        row = Math.round(row * 10) / 10; 
        totalDays[j][1] = Math.round(totalDays[j][1] * 10) / 10;
      }
      
      $('total_' + totalDays[j][0]).firstChild.nodeValue = totalDays[j][1];
    }
    $('total_' + i).firstChild.nodeValue = row;
    
    total += row;
  }

  total = Math.round(total * 10) / 10; 
  if ($('total_grand')) $('total_grand').firstChild.nodeValue = total;
}


/**
 * Highlight rows and columns
 */
function highlight(f, id, a) {
  var days = ['w1_sun', 'w1_mon', 'w1_tue', 'w1_wed', 'w1_thu', 'w1_fri', 'w1_sat', 
              'w2_sun', 'w2_mon', 'w2_tue', 'w2_wed', 'w2_thu', 'w2_fri', 'w2_sat'];     
  var id = id.split('-');
  var rowNum = id[3];
  var colNum = id[5];
  
  //don't count the timesheet fieldset
  var n = f.getElementsByTagName('fieldset').length - 1;

  //if there is the add project fieldset make sure we don't count that
  if ($('timesheet-add-project') != null) n--;

  for (var i = 0; i < n; i++) {
    for (var j = 0; j < days.length; j++) {
      w = $('cell-timesheet-project-' + i + '-days-' + days[j]);
      
      //highlight current cell mouse is hovering over
      if (i == rowNum && days[j] == colNum) {
        a == 'add' ? addClass(w, 'highlight current') : removeClass(w, 'highlight current');
      }
      //highlight all other row or column cells
      else if (i == rowNum || days[j] == colNum) {
        a == 'add' ? addClass(w, 'highlight') : removeClass(w, 'highlight');
      }
    }
  }
  
  //highlight the project cell
  x = $('cell-timesheet-project-' + rowNum);
  a == 'add' ? addClass(x, 'highlight') : removeClass(x, 'highlight');

  //if you aren't hovering over the project, highlight the total column cell  
  if (colNum) {
    y = $('total_' + colNum);
    a == 'add' ? addClass(y, 'highlight') : removeClass(y, 'highlight');
    u = $('date_' + colNum);
    a == 'add' ? addClass(u, 'highlight') : removeClass(u, 'highlight');
  }

  // highlight the total row cell
  z = $('total_' + rowNum);
  a == 'add' ? addClass(z, 'highlight') : removeClass(z, 'highlight');
}


/**
 * Submitting a timesheet
 */
function timesheetSubmit(f) {
  var element;
  for (var j = 0; element = f[j]; j++) {
    if (hasClass(element, 'error')) {
	   alert(inputErrorMessage);
	 	 return false;
	 }
  }
}