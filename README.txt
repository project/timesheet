Timesheet Module
---------------------------------------------------------------------------------------------------------------------
Authors: Theodore Serbinski (aka m3avrck) tserbinski [--at--] washsq [--dot--] com
         Earnest Berry (aka Souvent) eberry [--at--] washsq [--dot--] com
         

Written as part of an internal company timesheet project, released and maintained for the community.



DESCRIPTION
---------------------------------------------------------------------------------------------------------------------
This module allows the tracking of time per project in two week intervals.



DEPENDENCIES
---------------------------------------------------------------------------------------------------------------------
 - PHP5: utilizes PHP5's object oriented approach
 - MySQL 4.1+: needed for foreign key and subselect support (data integrity and summary reporting), along with transaction support, few minor MySQL specific queries
 - Project module: utilizes the same projects defined with project.module
 - FPDF: This enables PDF printing of timesheets
 


INSTALLATION
---------------------------------------------------------------------------------------------------------------------

PRE-INSTALLATION INSTRUCTIONS:
------------------------------
i.   Make sure Project module is installed and enabled
ii.  Make sure at least one Project has been created
iii. If you wish to use automatic holiday support, create a project that has the word 'holiday' somewhere in the title, case does not matter


INSTALLATION INSTRUCTIONS:
------------------------------
1. Place the entire timesheet directory into your Drupal modules/ directory

2. Download FPDF from: http://www.fpdf.org/

3. Extract the FPDF files into modules/timesheet/print/fpdf/ directory

4. Create the timesheet tables in your database using the supplied timesheet.mysql script

5. Enable module at: administer > modules

6. Configure module: administer > settings > timesheets



NOTES
---------------------------------------------------------------------------------------------------------------------
* If your timesheet and pay days aren't correct, load timesheet.inc and adjust these variables defined at the top.

* There are *no* plans to release a version of this module that will work with PHP4 and MySQL3 or MySQL4 so don't ask. Too many dependencies on newer and better technologies. But thanks for the thought! :)


