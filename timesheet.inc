<?php

/**
 * @file
 * timesheet class and additional helper functions
 */

/**
 * @name timesheet seed values
 * @{
 * Start dates for when timesheets are due and when payday is, 2 week intervals
 * based off of these seed values.
 */

//split up due date and time so we don't have to worry about DST issues
define('INITIAL_TIMESHEET_DATE', '2005-02-26');
define('TIMESHEET_END_TIME', '23:59:59');
define('INITIAL_PAYDAY_DATE', '2005-03-04');

/**
 * @} End of "timesheet seed values".
 */
 
 
/**
 * @class
 * @{
 */
class timesheet {

  private $_uid;
  private $_tid;
  private $_lid;
  private $_default_hours;
  private $_timesheet;
  private $_submit_date;
  private $_approve_date;
  private $_end_date;
  private $_days_left;    
  
  
  /**
   * @constructor
   * @param $uid user id to associate with timesheet
   * @param $tid timesheet id to load (if any)
   * @param $options associative array of options: timesheet_default_labor_category =>  N, timesheet_default_hours => N, timesheet_default_projects => [['nid' => N, 'lid' => N], etc...]
   */
  public function __construct($uid, $tid, $options) {
    if (isset($uid) && is_numeric($uid)) {
      $this->_uid = $uid;
      $lid = db_result(db_query('SELECT MIN(lid) FROM {timesheet_labor_categories}'));
      $this->_lid = is_numeric($options['timesheet_default_labor_category']) ? $options['timesheet_default_labor_category'] : $lid;
      $this->_default_hours = is_numeric($options['timesheet_default_hours']) ? $options['timesheet_default_hours'] : 80;
    
      if ($tid != '') {
        //make sure the tid passed in is valid
        $tid_check = db_result(db_query('SELECT tid FROM {timesheet} WHERE uid = %d AND tid = %d', $this->_uid, $tid));

        if (is_numeric($tid_check)) {
          $this->_tid = $tid_check;
          $this->_load();
        }
        else {
          $this->_tid = 0;
        }
      }
      else {    
        $this->_set_end_date();
        //check to see if the user already has a timesheet and load that
        $tid = db_result(db_query('SELECT tid FROM {timesheet} t INNER JOIN {timesheet_intervals} i ON t.did = i.did WHERE t.uid = %d AND i.end_date = \'%s\'', $this->_uid, $this->_end_date));

        if ($tid != '') {
          $this->_tid = $tid;
          $this->_load();
        }
        //otherwise create a new timesheet for the user
        else {
          $this->_create($options['timesheet_default_projects']);
          $this->_check_holidays();
          $this->save($this->_timesheet);
       }
      }
    }
  }
	
  
  /**
   * @return user id for timesheet
   */
  public function get_uid() {
    return $this->_uid;
  }
  
  
  /**
   * @return default labor category id for timesheet
   */
  public function get_lid() {
    return $this->_lid;
  }
 
  /**
   * @return the submit date formatted as: 2005-10-10 23:59:59
   */
  public function get_submit_date() {
    return $this->_submit_date;
  }
  
  
  /**
   * @return the approve date formatted as: 2005-10-10 23:59:59
   */
  public function get_approve_date() {
    return $this->_approve_date;
  }
  
   
  /**
   * @return the end date formatted as: 2005-10-10 23:59:59
   */
  public function get_end_date() {
    return $this->_end_date;
  }
  
  
  /**
   * @return number of days left
   */
  public function get_days_left() {
    return $this->_days_left;
  }
  
  
  /**
   * @return the current timesheet as an associate array
   */
  public function get_timesheet() {
    return $this->_timesheet;
  }
  
	
	/**
	 * @return the current timesheet id
	 */
	public function get_tid() {
		return $this->_tid;
	}
  
    
  /**
   * Adds a project (or a row) to a timesheet
   * @param $nid project id
   * @param $lid labor category id
   * @param $on_site on site boolean value
   * @return true or false whether or not the project was added
   */
  public function add_project($nid, $lid, $on_site) {
    if (is_numeric($nid) && is_numeric($lid) && is_numeric($on_site)) {

      //make sure this project and labor category don't already exist, don't want duplicates of the same entry
			if(is_array($this->_timesheet)){
				foreach ($this->_timesheet AS $project) {
					if ($project['nid'] == $nid && $project['lid'] == $lid && $project['on_site'] == $on_site) {
						return false;
					}
				}
			}
    
      //no duplicates found, add the project
      $this->_timesheet[] = array('nid' => $nid,
                                  'lid' => $lid,
                                  'on_site' => $on_site,
                                  'w1_sun' => 0.0,
                                  'w1_mon' => 0.0,                              
                                  'w1_tue' => 0.0,
                                  'w1_wed' => 0.0,
                                  'w1_thu' => 0.0,
                                  'w1_fri' => 0.0,
                                  'w1_sat' => 0.0,
                                  'w2_sun' => 0.0,
                                  'w2_mon' => 0.0,
                                  'w2_tue' => 0.0,
                                  'w2_wed' => 0.0,
                                  'w2_thu' => 0.0,
                                  'w2_fri' => 0.0,
                                  'w2_sat' => 0.0
                                  );
                                  
      return true;
    }
    else {
      return false;
    }
  }
  
  
  /**
   * Deletes a project (or a row) from a timesheet
   * Note there can be multiple projects as long as their labor categories are different
   * @param $nid project id
   * @param $lid labor category id
   * @param $on_site on site boolean
   */
  public function delete_project($nid, $lid, $on_site) {
    foreach ($this->_timesheet AS $key => $project) {
      if ($project['nid'] == $nid && $project['lid'] == $lid && $project['on_site'] == $on_site) {
        unset($this->_timesheet[$key]);
      }
    }
  }
  
  
  /**
   * Save a timesheet
   * @param $timesheet the timesheet array to save, either from $this->_timesheet or from user input in the module
   * @return boolean on successful save or not
   */
  public function save($timesheet) {
    if (is_array($timesheet)) {
      
      //no queries have failed yet
      $fail = false;
      
      //make this query transaction safe since we have to delete rows, don't want to mess up the database    
 			db_query('START TRANSACTION');
    
      //delete all projects associated with this timesheet because we will rewrite thesse in
      //no way to really update if a user adds/deletes projects
      $result = db_query('SELECT xid FROM {timesheet_timesheet_times} WHERE tid = %d', $this->_tid);
    
      while ($row = db_fetch_array($result)) {
        $try = db_query('DELETE FROM {timesheet_times} WHERE xid = %d', $row['xid']);
        
        //if the query failed no use continuing in the loop
        if (!$try) {
          $fail = true;
          break;
        }
      }
    	
      foreach ($timesheet AS $project) {
      	$try01 = db_query('INSERT INTO {timesheet_times} (nid, lid, on_site, w1_sun, w1_mon, w1_tue, w1_wed, w1_thu, w1_fri, w1_sat, w2_sun, w2_mon, w2_tue, w2_wed, w2_thu, w2_fri, w2_sat) 
                           VALUES (%d, %d, %d, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f)', $project);
        
        $try02 = db_query('INSERT INTO {timesheet_timesheet_times} (tid, xid) VALUES (%d, last_insert_id())', $this->_tid);
        
        //if one the queries failed no use continuing in the loop
        if (!$try01 || !$try02) {
          $fail = true;
          break;
        }
        
      }
      
      //if any of the queries failed, rollback
			if ($fail) {
				db_query('ROLLBACK');
				return false;
			}
      else {
        db_query('COMMIT');
        $this->_timesheet = $timesheet;
        return true;
      }
    }
  }

  
  /**
   * Submit a timesheet
   * @return true on successful submit or false
   */
  public function submit() {
    $now = date('Y-m-d H:i:s');
    db_query('UPDATE {timesheet} SET submit_date = \'%s\' WHERE tid = %d', $now, $this->_tid);
    $this->_submit_date = $now;
  }
  
  
  /**
   * Approve a timesheet
   */
  public function approve() {
    $now = date('Y-m-d H:i:s');
    db_query('UPDATE {timesheet} SET approve_date = \'%s\' WHERE tid = %d', $now, $this->_tid);
    $this->_approve_date = $now;
  }
 
  
  /**
   * Creates a new timesheet
   */
  private function _create($projects) {
    $this->_timesheet = array();

    if (is_array($projects) && count($projects) > 0) {
      foreach ($projects AS $project) {
        $this->add_project($project['nid'], $project['lid'], $project['on_site']);
      }
    }
    
    $did = db_result(db_query('SELECT did FROM {timesheet_intervals} WHERE end_date = \'%s\'', $this->_end_date));
    
    db_query('INSERT INTO {timesheet} (uid, did) VALUES (%d, %d)', $this->_uid, $did);
    
    //instead of locking table and using SELECT last_insert_id() this is far better and less error prone
    $this->_tid = db_result(db_query('SELECT tid FROM {timesheet} WHERE uid = %d AND did = %d', $this->_uid, $did));
  }
  
  
  /**
   * Loads a timesheet from the database
   */
  private function _load() {
    $result = db_query('SELECT * FROM {timesheet_times} t INNER JOIN {timesheet_timesheet_times} x ON t.xid = x.xid WHERE x.tid = %d', $this->_tid);
    
    while ($row = db_fetch_array($result)) {
      $this->_timesheet[] = array('nid' => $row['nid'],
                                  'lid' => $row['lid'],
                                  'on_site' => $row['on_site'],
                                  'w1_sun' => $row['w1_sun'],
                                  'w1_mon' => $row['w1_mon'],                              
                                  'w1_tue' => $row['w1_tue'],
                                  'w1_wed' => $row['w1_wed'],
                                  'w1_thu' => $row['w1_thu'],
                                  'w1_fri' => $row['w1_fri'],
                                  'w1_sat' => $row['w1_sat'],
                                  'w2_sun' => $row['w2_sun'],
                                  'w2_mon' => $row['w2_mon'],
                                  'w2_tue' => $row['w2_tue'],
                                  'w2_wed' => $row['w2_wed'],
                                  'w2_thu' => $row['w2_thu'],
                                  'w2_fri' => $row['w2_fri'],
                                  'w2_sat' => $row['w2_sat']
                                  );
    }
    
    $result = db_fetch_array(db_query('SELECT t.submit_date, t.approve_date, i.end_date FROM {timesheet} t INNER JOIN {timesheet_intervals} i ON t.did = i.did WHERE t.tid = %d', $this->_tid));
    $this->_submit_date = $result['submit_date'];
    $this->_approve_date = $result['approve_date'];
    $this->_end_date = $result['end_date'];
    
    $now = time();
    $time_left = strtotime($this->_end_date) - $now;
    
    if ($time_left <= 0) {
      $this->_days_left = 0;
    }
    else {
      $this->_days_left = 14 - ((($now - strtotime(INITIAL_TIMESHEET_DATE)) / 86400) % 14);
    }
  }
  
  
  /**
   * Figures out when the end date is for the current timesheet, based on a 2 week interval from the seed date and time
   */
  private function _set_end_date() {
    $init_time = strtotime(INITIAL_TIMESHEET_DATE);
    $now = time();
    
    //find out days left till timesheet due, 1 day = 86400 secs
    $this->_days_left = 14 - ((($now - $init_time) / 86400) % 14);
    list($hour,$min,$sec) = explode(':' , TIMESHEET_END_TIME);
    
    //use mktime to add days for automatic month/year rollover and also account for DST
    $this->_end_date = mktime($hour, $min, $sec, date('m'), date('d') + $this->_days_left, date('Y'));

    $this->_end_date = date('Y-m-d H:i:s', $this->_end_date);
    
    $did = db_result(db_query('SELECT did FROM {timesheet_intervals} WHERE end_date = \'%s\'', $this->_end_date));
    
    //insert _end_date if it isn't in the timesheet_intervals table
    if (!$did) {
      db_query('INSERT INTO {timesheet_intervals} (end_date) VALUES (\'%s\')', $this->_end_date);
    }
  }

  
  
  /**
   * Adds any holidays to the timesheet
   */
  private function _check_holidays() {
    $year = date('Y');
    $rs = db_query("SELECT * FROM {timesheet_holidays}");
    
    while($holiday = db_fetch_object($rs)) {
      $digest = split(",", $holidays->hl_formula);
      $holidays[$holiday->hl_title] = strtotime($digest[0], $digest[1]);
    }
    /*
    $holidays = array("New Year's" => strtotime("1 January $year"),
                      "President's Day" => strtotime("3 week Monday", strtotime("1 February $year")),
                      "Memorial Day" => strtotime("-1 week Monday", strtotime("1 June $year")),
                      "Independence Day" => strtotime("4 July $year"),
                      "Labor Day" => strtotime("1 week Monday", strtotime("1 September $year")),
                      "Columbus Day" => strtotime("1 week Monday", strtotime("1 October $year")),
                      "Veteran's Day" => strtotime("11 November $year"),
                      "Thanksgiving" => strtotime("3 week Thursday", strtotime("1 November $year")),
                      "Christmas" => strtotime("25 December $year")
                      );
    */                  
    //get the holiday project id from database
    // TODO
    // Change this to get the nid of the associated holiday node, not a project.
    $holiday_nid = db_result(db_query("SELECT nid FROM {node} WHERE type = 'project_project' AND title LIKE '%holiday%'"));
    $holiday_nid = variable_get('timesheet_holiday_node', '');

    //if a holiday project hasn't been created, no need to continue, holiday support is disabled
    if ($holiday_nid == '') {
      return;
    }
     
    //find the start and end dates for this timesheet 2 week period
    list($date, $time) = explode(' ', $this->_end_date);
    list($year, $month, $day) = explode('-', $date);
    list($hour, $min, $sec) = explode(':', $time);
     
    //use mktime to subtract 2 weeks for start date for automatic month/year rollover and also account for DST
    $start_time = mktime($hour, $min, $sec, $month, $day - 14, $year);
    $end_time = strtotime($this->_end_date);
     
    //loop through all holidays and see if they fall in between the start and end times
    foreach ($holidays AS $holiday => $time) {
      if ($time >= $start_time && $time <= $end_time) {
        $this->add_project($holiday_nid, $this->_lid, 0);

        //how many days did the holiday occur after the start date of the timesheet
        $days = floor(($time - $start_time) / 86400);

        //update this project by automatically filling in default_hours / 10 (estimate of # of hours worked per day per timesheet, usually 8 or 9)
        //offset of 3 because [0]=nid , [1]=lid , [2]=onsite in the timesheet array
        foreach ($this->_timesheet AS $key => $project) {
          if ($project['nid'] == $holiday_nid) {
 	          $keys = array_keys($this->_timesheet[$key]);
            $this->_timesheet[$key][$keys[3 + $days]] = sprintf('%.1f', $this->_default_hours / 10);
          }
        }
      }
    }
    return true;
  }
}

/**
 * @} End of class
 */



/*****************************************************************************************************************************/




/**
 * @functions
 * Additional helper functions to use in addition to timesheet objects and methods
 */


/**
 * Find all unsubmitted timesheets and create entries in database for any not already in there
 * @param $uid the user id to find and create all unsubmitted timesheets for
 * @param $created the creation date of the user account, don't need to find unsubmitted timesheets for when the user wasn't even created yet
 * @return a multidimensional array where each row is array('tid' => N, 'end_date' => 2005-10-10 23:59:59)
 */
function timesheet_get_unsubmitted($uid, $created) {
  $unsubmitted = array();
  
  if (is_numeric($uid)) {
    //find all end date ids for where a user has not either created a timesheet or hasn't submitted their timesheet
    //don't select any timesheets where their end date is in the future
    $result = db_query('SELECT i.did, i.end_date FROM {timesheet_intervals} i LEFT JOIN (SELECT * FROM {timesheet} WHERE uid = %d) t ON i.did = t.did WHERE t.submit_date IS NULL AND i.end_date < CURDATE() AND unix_timestamp(i.end_date) > %d', $uid, $created);
    
    while ($row = db_fetch_array($result)) {
      //find out if the user already has a timesheet in the database for this period, don't create duplicates
      $tid = db_result(db_query('SELECT tid FROM {timesheet} WHERE uid = %d AND did = %d', $uid, $row['did']));
      
      //no timesheet found, so insert a blank one for the unsubmitted time period
      if ($tid == '') {
        db_query('INSERT INTO {timesheet} (uid, did) VALUES (%d, %d)', $uid, $row['did']);
        $tid = db_result(db_query('SELECT last_insert_id()'));
      }
      
      //update unsubmitted array
      $unsubmitted[] = array('tid' => $tid, 'end_date' => $row['end_date']);
    }
  }
  return $unsubmitted;
}


/**
 * Find all timesheets within optional range
 * @param $uid a single user id or an array of user ids to find timesheets for e.g., array(1, 3, 5)
 * @param $range an array of start and end month/year combos: array('start_day' => 1, 'start_month' => 1, 'end_year' => 2005 ...
 * @param $status what status values to look for: submitted, approved, neither, any
 * @param $raw_sql boolean to return just the SQL for use in pager queries or false to return the results array
 * @return a multidimensional array where each row is array('tid' => N, 'end_date' => 2005-10-10 23:59:59)
 */
function timesheet_get_history($uid, $range, $status = 'submitted', $raw_sql = TRUE) {
  $history = array();
  $params = array();
 
  if (is_numeric($uid)) {
    $uid = array($uid);
  }

  //find all timesheets
  $sql = 'SELECT t.tid, t.uid, t.submit_date, t.approve_date, i.end_date 
          FROM {timesheet} t 
          INNER JOIN {timesheet_intervals} i ON t.did = i.did WHERE 1';

  //if we have an array of uids adjust sql, otherwise we'll find all users
  if (is_array($uid)) {
    $sql .=  ' AND (0';
   
    foreach ($uid AS $user) {
      $sql .= ' OR t.uid = %d';
      $params[] = $user;
    }
    
    $sql .= ') ';
  }
  
  if ($status == 'submitted') {
    $sql .= ' AND t.submit_date IS NOT NULL ';
  }
  else if ($status == 'approved') {
    $sql .= ' AND t.approve_date IS NOT NULL ';
  }
  else if ($status == 'neither') {
    $sql .= ' AND t.submit_date IS NULL AND t.approve_date IS NULL ';
  }

  //set mktime to start at the 0th hour and end at the 23rd hour so it doesn't matter what the TIMESHEET_END_TIME is set to
  if (is_numeric($range['start_day']) && is_numeric($range['start_month']) && is_numeric($range['start_year'])) {
    $sql .= ' AND i.end_date >= \'%s\'';
    $params[] = date('Y-m-d H:i:s', mktime(0, 0, 0, $range['start_month'], $range['start_day'], $range['start_year']));
  }
  
  if (is_numeric($range['end_day']) && is_numeric($range['end_month']) && is_numeric($range['end_year'])) {
    $sql .= ' AND i.end_date <= \'%s\'';
    $params[] = date('Y-m-d H:i:s', mktime(23, 59, 59, $range['end_month'], $range['end_day'], $range['end_year']));
  }

  if ($raw_sql) {
     if (is_array($params)) {
       $args = array_merge(array($sql), $params);
     }
     $args = array_map('db_escape_string', $args);
     $args[0] = $sql;
     $sql = call_user_func_array('sprintf', $args); 

     return $sql;
  }
  else {
    $result = db_query($sql, $params);

    while ($row = db_fetch_array($result)) {
      $history[] = $row;
    }

    return $history;
  }
}


/**
 * Calculate various statistics for certain users/time periods
 * @param $uid a single user id or an array of user ids to find stats for e.g., array(1, 3, 5)
 * @param $nid a single project id or an array of user project ids to find stats for e.g., array(1, 3, 5)
 * @param $range an array of start and end month/year combos: array('start_day' => 1, 'start_month' => 1, 'end_year' => 2005 ...
 * @return a multidimensional array where each row corresponds to a user, each column a project, and each cell a total hours for that user for that project, along with grand total for each row
 */
function timesheet_get_statistics($uid, $nid, $range = '') {
  $stats = array();
  $params = array();

  if (is_numeric($uid)) {
    $uid = array($uid);
  }
  if (is_numeric($nid)) {
    $nid = array($nid);
  }
  
  //if uid or nid aren't arrays or if the uid array or nid array is empty we can't build stats
  if (!is_array($uid) || !is_array($nid) || count($uid) < 1 || count($nid) < 1) {
    return false;
  }
    
  //find total number of hours each user worked on each project
  $sql = 'SELECT uid, nid, (w1_sun + w1_mon + w1_tue + w1_wed + w1_thu + w1_fri + w1_sat + w2_sun + w2_mon + w2_tue + w2_wed + w2_thu + w2_fri + w2_sat) AS total
          FROM timesheet t
          INNER JOIN timesheet_intervals i ON t.did = i.did
          INNER JOIN timesheet_timesheet_times tx ON t.tid = tx.tid
          INNER JOIN timesheet_times x ON tx.xid = x.xid
          WHERE (0';
   
  foreach ($uid AS $user) {
    $sql .= ' OR t.uid = %d';
    $params[] = $user;
  }
  
  $sql .= ' ) AND (0';
  
  foreach ($nid AS $project) {
    $sql .= ' OR x.nid = %d';
    $params[] = $project;
    
    //build stats array
    foreach ($uid AS $user) {
      $stats[$user][$project] = 0;
    }
    
    $stats['Total'][$project] = 0;
  }
  
  $sql .= ') ';
  
  //set mktime to start at the 0th hour and end at the 23rd hour so it doesn't matter what the TIMESHEET_END_TIME is set to
  if (is_numeric($range['start_day']) && is_numeric($range['start_month']) && is_numeric($range['start_year'])) {
    $sql .= ' AND i.end_date >= \'%s\'';
    $params[] = date('Y-m-d H:i:s', mktime(0, 0, 0, $range['start_month'], $range['start_day'], $range['start_year']));
  }
  
  if (is_numeric($range['end_day']) && is_numeric($range['end_month']) && is_numeric($range['end_year'])) {
    $sql .= ' AND i.end_date <= \'%s\'';
    $params[] = date('Y-m-d H:i:s', mktime(23, 59, 59, $range['end_month'], $range['end_day'], $range['end_year']));
  }

  $result = db_query($sql, $params);
  
  while ($row = db_fetch_array($result)) {
    $stats[$row['uid']][$row['nid']] += $row['total'];
    $stats['Total'][$row['nid']] += $row['total'];
  }

  return $stats;
}


/**
 * @return an array of timestamp fields for the last timesheet interval
 */
function timesheet_get_last_interval() {
  $interval = db_result(db_query('SELECT MAX(end_date) FROM {timesheet_intervals}'));

  list($date, $time) = explode(' ', $interval);
  list($year, $month, $day) = explode('-', $date);
  list($hour, $min, $sec) = explode(':', $time);
  
  return array('year' => $year, 'month' => $month, 'day' => $day, 'hour' => $hour, 'min' => $min, 'sec' => $sec);
}

 
/**
 * @param $lid specific labor category to return, otherwise all labor categories are returned
 * @return a list of all labor categories associated with timesheets
 */
function timesheet_get_labor_categories($lid = '') {
  if ($lid == '') {
    $labor_categories = array();
    
    $result = db_query('SELECT * FROM {timesheet_labor_categories} ORDER BY name');
    
    while ($row = db_fetch_array($result)) {
      $labor_categories[$row['lid']] = $row['name'];
    }
  }  
  else if (is_numeric($lid)) {
    $labor_categories = db_fetch_array(db_query('SELECT lid, name FROM {timesheet_labor_categories} WHERE lid = %d', $lid));
  }
  
  return $labor_categories;
}


/**
 * @param $name the name of the labor category to add
 * @return true on successful insertion, otherwise return error code
 */
function timesheet_add_labor_categories($name) {
  $check = false;

  //if $name is set and it starts with an alphanumeric character and has any combo of alphanumeric chars, spaces, ',' and '.', '-', '/'
  if (isset($name) && preg_match('/^[\w]+[\w ,.-\/]*$/', $name)) {
    //check for duplicates, MySQL won't allow it but we don't want those nasty errors
    $lid_check = db_result(db_query('SELECT lid FROM {timesheet_labor_categories} WHERE name = \'%s\'', $name));
    
    if ($lid_check == '') {
      db_query('INSERT INTO {timesheet_labor_categories} (name) VALUES (\'%s\')', $name);
      $check = true;
    }
    else {
      $check = 'duplicate';
    }
  }
  else {
    $check = 'invalid';
  }

  return $check;
}


/**
 * @param $lid the id of the labor category to delete
 * @return true on successful deletion, otherwise false on error
 */
function timesheet_delete_labor_categories($lid) {
  //make sure lid is set and numeric
  if (isset($lid) && is_numeric($lid)) {
    return db_query('DELETE FROM {timesheet_labor_categories} WHERE lid = %d', $lid);
  }
  else {
    return false;
  }
}


/**
 * @param $lid the id of the labor category to update
 * @param $name the new name for the labor category with id $lid
 * @return true on successful update, otherwise false on error
 */
function timesheet_update_labor_categories($lid, $name) {
  //make sure lid is set and numeric
  //make sure name is set and it starts with an alphanumeric character and has any combo of alphanumeric chars, spaces, ',' and '.', '-', '/'
  if (isset($lid) && is_numeric($lid) && isset($name) && preg_match('/^[\w]+[\w ,.-\/]*$/', $name)) {
    return db_query('UPDATE {timesheet_labor_categories} SET name = \'%s\' WHERE lid = %d', $name, $lid);
  }
  else {
    return false;
  }
}


/**
 * @param $nid specific project to return, otherwise all projects are returned
 * @return a list of all project ids and names
 */
function timesheet_get_projects($nid = '') {
  $projects = array();
  if ($nid == '') {
    $nodes = array();
    # Get avail node types
    $cur_types = node_get_types();
    foreach($cur_types as $type => $name) {
      if(variable_get("timesheet_avail_node_types_$type", "")) {
        $filtered_types[] = $type;
      }
    }
    if(count($filtered_types) == 0) {
      return $nodes;
    }
    
    $filtered_types = "'" . implode("','", $filtered_types) . "'";
    // TODO Group them like in taxonomy by node type
    $rs = db_query("SELECT n.nid, n.title, n.type
      FROM {node} n INNER JOIN {timesheet_enabled_nodes} tsen ON n.nid = tsen.nid
      WHERE n.type IN ($filtered_types)");
    while($row = db_fetch_object($rs)) {
      $nodes[$row->type][$row->nid] = $row->title;
    }

  }
  else if (is_numeric($nid)) {
    $nodes = db_fetch_array(db_query('SELECT nid, title FROM {node} WHERE nid = %d', $nid));
  }
  
  return $nodes;  
}


/**
 * @param $uid specific user to return, otherwise all users are returned (except for the anonymous user 0)
 * @return a list of all uids and names
 */
function timesheet_get_users($uid = '') {
  if ($uid == '') {
    $users = array();
    
    $result = db_query('SELECT uid, name FROM {users} WHERE uid > 0');

    while ($row = db_fetch_array($result)) {
     $users[] = $row;
    }
  }
  else if (is_numeric($uid)) {
    $users = db_result(db_query('SELECT name FROM {users} WHERE uid = %d', $uid));
  }
  
  return $users; 
}


/**
 * @param $header an array of table headers
 * @param $rows an array of rows in the table
 * @param $format file format of export, default is CSV
 * @param $filename name of file to export results as
 */
function timesheet_export_statistics($header, $rows, $format = 'CSV', $filename) {
  $file = '';

  switch ($format) {
    case 'CSV':
    default:
      foreach ($header AS $cell) {
        $file .= $cell['data'] . ', ';
      }
      
      $file .= "\n";
      
      foreach ($rows AS $row) {
        foreach ($row['data'] AS $cell) {
          $file .= $cell['data'] . ', ';
        }
        $file .= "\n";
      }
      
      header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
      header('Content-Type: text/csv');
      header('Content-Disposition: attachment; filename="' . $filename . '"');
      break;
  }
  
  print $file;
  exit;
}
